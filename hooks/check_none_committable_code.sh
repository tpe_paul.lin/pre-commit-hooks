#!/usr/bin/env bash

result=$(git commit -v --dry-run | grep 'NO-COMMIT' 2>&1)
if [[ $result != '' ]]; then
  echo "Trying to commit non-committable code: '$result'"
  exit 1
else
    exit 0
fi
